"use strict";

var _interopRequireDefault = require("@babel/runtime-corejs3/helpers/interopRequireDefault");

require("core-js/modules/es.symbol");

require("core-js/modules/es.symbol.description");

require("core-js/modules/es.symbol.iterator");

require("core-js/modules/es.array.iterator");

require("core-js/modules/es.number.constructor");

require("core-js/modules/es.object.define-property");

require("core-js/modules/es.object.to-string");

require("core-js/modules/es.regexp.exec");

require("core-js/modules/es.string.iterator");

require("core-js/modules/es.string.replace");

require("core-js/modules/web.dom-collections.iterator");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.EnvVars = void 0;

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/createClass"));

var _indexOf = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/index-of"));

var _parseInt2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-int"));

var _trunc = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/math/trunc"));

var _parseFloat2 = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/parse-float"));

var _trim = _interopRequireDefault(require("@babel/runtime-corejs3/core-js-stable/instance/trim"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime-corejs3/helpers/defineProperty"));

var booleanTrueValues = ['true', 't', '1', 'on', 'enable', 'enabled'];
var booleanFalseValues = ['false', 'f', '0', 'off', 'disable', 'disabled'];

var EnvVars =
/*#__PURE__*/
function () {
  function EnvVars() {
    var env = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : process.env;
    (0, _classCallCheck2.default)(this, EnvVars);
    this.env = env;
    (0, _defineProperty2.default)(this, "throwOrLogCastError", void 0);
    (0, _defineProperty2.default)(this, "castErrorBreakMatches", true);
    (0, _defineProperty2.default)(this, "strictCastNumbers", true);
  }
  /**
   * Throw or log cast error
   */


  (0, _createClass2.default)(EnvVars, [{
    key: "castValueTo",
    value: function castValueTo(names, type, defaultValue) {
      if (typeof names === 'string') {
        return this.castValueTo([names], type, defaultValue);
      }

      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = names[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var _name = _step.value;
          var _value = this.env[_name];

          if (_value != null) {
            if (type === 'value') {
              return _value;
            }

            if (type !== 'json') {
              var stringValue = (0, _trim.default)(_value).call(_value).replace(/^"(.+)"$/, '$1');

              if (type === 'number' || type === 'integer') {
                var _number = this.strictCastNumbers ? Number(stringValue) : (0, _parseFloat2.default)(stringValue);

                if (!isNaN(_number)) {
                  if (type === 'number') {
                    return _number;
                  }

                  if (type === 'integer') {
                    if (!this.strictCastNumbers) {
                      return typeof _trunc.default === 'function' ? (0, _trunc.default)(_number) : (0, _parseInt2.default)("".concat(_number), 10);
                    }

                    if (_number === (0, _parseInt2.default)(stringValue, 10)) {
                      return _number;
                    }

                    if (this.throwOrLogCastError != null) {
                      this.throwOrLogCastError(new Error('Expected integer value'), type, _name, stringValue);
                    }

                    if (this.castErrorBreakMatches) {
                      break;
                    }
                  }
                } else {
                  if (this.throwOrLogCastError != null) {
                    this.throwOrLogCastError(new Error('Invalid number'), type, _name, stringValue);
                  }

                  if (this.castErrorBreakMatches) {
                    break;
                  }
                }
              }

              if (type === 'boolean') {
                var booleanValue = stringValue.toLowerCase();

                if ((0, _indexOf.default)(booleanTrueValues).call(booleanTrueValues, booleanValue) >= 0) {
                  return true;
                }

                if ((0, _indexOf.default)(booleanFalseValues).call(booleanFalseValues, booleanValue) >= 0) {
                  return false;
                }

                if (this.throwOrLogCastError != null) {
                  this.throwOrLogCastError(new Error('Not valid boolean value'), type, _name, stringValue);
                }

                if (this.castErrorBreakMatches) {
                  break;
                }
              }
            } else {
              try {
                return JSON.parse(_value);
              } catch (error) {
                if (this.throwOrLogCastError != null) {
                  this.throwOrLogCastError(error, type, _name, _value);
                }

                if (this.castErrorBreakMatches) {
                  break;
                }
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return != null) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      return defaultValue;
    }
    /**
     * Gets a original string environment variable by their given name.
     *
     * @param  {String} name - The name of the environment variable.
     * @param  {String} defaultVal - The default value to use.
     *
     * @return {String} The value.
     */

  }, {
    key: "value",
    value: function value(names, defaultVal) {
      return this.castValueTo(names, 'value', defaultVal);
    }
    /**
     * Gets a string environment variable by their given name.
     *
     * @param  {String} name - The name of the environment variable.
     * @param  {String} defaultVal - The default value to use.
     *
     * @return {String} The value.
     */

  }, {
    key: "string",
    value: function string(names, defaultVal) {
      return this.castValueTo(names, 'string', defaultVal);
    }
    /**
     * Gets a number environment variable by their given name.
     * Notice: It do not verify NaN
     *
     * @param  {String} name - The name of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */

  }, {
    key: "number",
    value: function number(names, defaultVal) {
      return this.castValueTo(names, 'number', defaultVal);
    }
    /**
     * Gets a integer environment variable by their given name.
     * @param  {String} name - The name of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */

  }, {
    key: "int",
    value: function int(names, defaultVal) {
      return this.castValueTo(names, 'integer', defaultVal);
    }
    /**
     * Gets a boolean environment variable by their given name.
     * @param  {String} name - The name of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */

  }, {
    key: "bool",
    value: function bool(names, defaultVal) {
      return this.castValueTo(names, 'boolean', defaultVal);
    }
    /**
     * Parses a environment variable as json
     * @param name - The name of the environment variable.
     * @param defaultVal - The default value to use.
     * @param [throwOrLogError = false] - If should throw parsing error
     *
     * @return The value.
     */

  }, {
    key: "json",

    /**
     * Parses a environment variable as json
     * @param names - The names of the environment variable.
     * @param defaultVal - The default value to use.
     * @param [throwOrLogError = false] - If should throw parsing error
     *
     * @return The value.
     */
    value: function json(names, defaultVal) {
      return this.castValueTo(names, 'json', defaultVal);
    }
  }]);
  return EnvVars;
}();

exports.EnvVars = EnvVars;
//# sourceMappingURL=index.js.map