export interface CastMap {
    value: string;
    string: string;
    integer: number;
    number: number;
    boolean: boolean;
    json: any;
}
export interface ThrowOrLogCastErrorFallback {
    (error: Error, type: keyof CastMap, name: string, value: string): void;
}
export declare class EnvVars {
    readonly env: {
        [name: string]: string | undefined;
    };
    constructor(env?: {
        [name: string]: string | undefined;
    });
    /**
     * Throw or log cast error
     */
    throwOrLogCastError?: ThrowOrLogCastErrorFallback;
    /**
     * Rather it should break name matches and return default value in case of a casting error
     */
    castErrorBreakMatches: boolean;
    /**
     * Rather number casting should be strict
     */
    strictCastNumbers: boolean;
    protected castValueTo<K extends keyof CastMap, T extends CastMap[K] | undefined = CastMap[K] | undefined>(names: string | string[], type: K, defaultValue?: T): CastMap[K] | T;
    /**
     * Gets a original string environment variable by their given name.
     *
     * @param  {String} name - The name of the environment variable.
     * @param  {String} defaultVal - The default value to use.
     *
     * @return {String} The value.
     */
    value<T extends string | undefined = string | undefined>(name: string, defaultVal?: T): string | T;
    /**
     * Gets a original string environment variable by their given names (in matching order).
     *
     * @param  {String} names - The names of the environment variables.
     * @param  {String} defaultVal - The default value to use.
     *
     * @return {String} The value.
     */
    value<T extends string | undefined = undefined>(names: string | string[], defaultVal?: T): string | T;
    /**
     * Gets a string environment variable by their given name.
     *
     * @param  {String} name - The name of the environment variable.
     * @param  {String} defaultVal - The default value to use.
     *
     * @return {String} The value.
     */
    string<T extends string | undefined = string | undefined>(name: string, defaultVal?: T): string | T;
    /**
     * Gets a string environment variable by their given names (in matching order).
     *
     * @param  {String} names - The names of the environment variables.
     * @param  {String} defaultVal - The default value to use.
     *
     * @return {String} The value.
     */
    string<T extends string | undefined = undefined>(names: string | string[], defaultVal?: T): string | T;
    /**
     * Gets a number environment variable by their given name.
     * Notice: It do not verify NaN
     *
     * @param  {String} name - The name of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */
    number<T extends number | undefined = number | undefined>(name: string, defaultVal?: T): number;
    /**
     * Gets a number environment variable by their given  names (in matching order).
     * Notice: It do not verify NaN
     *
     * @param  {String} names - The name of the environment variables.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */
    number<T extends number | undefined = number | undefined>(names: string | string[], defaultVal?: T): number | T;
    /**
     * Gets a integer environment variable by their given name.
     * @param  {String} name - The name of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */
    int<T extends number | undefined = number | undefined>(name: string, defaultVal?: T): number | T;
    /**
     * Gets a integer environment variable by their given name. (in matching order).
     * @param  {String} names - The names of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */
    int<T extends number | undefined = number | undefined>(names: string | string[], defaultVal?: T): number | T;
    /**
     * Gets a boolean environment variable by their given name.
     * @param  {String} name - The name of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */
    bool<T extends boolean | undefined = boolean | undefined>(name: string, defaultVal?: T): boolean | T;
    /**
     * Gets a boolean environment variable by their given name. (in matching order).
     * @param  {String} names - The names of the environment variable.
     * @param  {number} defaultVal - The default value to use.
     *
     * @return {number} The value.
     */
    bool<T extends boolean | undefined = boolean | undefined>(names: string | string[], defaultVal?: T): boolean | T;
    /**
     * Parses a environment variable as json
     * @param name - The name of the environment variable.
     * @param defaultVal - The default value to use.
     * @param [throwOrLogError = false] - If should throw parsing error
     *
     * @return The value.
     */
    json(name: string, defaultVal?: any): any;
}
