/// <reference types="jest" />
import { EnvVars } from '..';

describe('Config', () => {
  const environments: any = {
    LISTEN_PORT: '80whatever',
    HTTP_LISTEN_PORT: ' "80" ',
  };

  const envVars = new EnvVars(environments);

  it('Default behavior', async () => {
    expect(envVars.env['LISTEN_PORT']).toBeDefined();
    expect(envVars.env['HTTP_LISTEN_PORT']).toBeDefined();
    expect(envVars.int('LISTEN_PORT', 3000)).toBe(3000);
    expect(envVars.int('HTTP_LISTEN_PORT', 3000)).toBe(80);
    expect(envVars.int(['UNDEFINED_VAR', 'HTTP_LISTEN_PORT', 'LISTEN_PORT'], 3000)).toBe(80);
    expect(envVars.int(['LISTEN_PORT', 'HTTP_LISTEN_PORT'], 3000)).toBe(3000);
  });

  it('Cast Error Break Matches', async () => {
    envVars.castErrorBreakMatches = false;
    expect(envVars.int(['LISTEN_PORT', 'HTTP_LISTEN_PORT'], 3000)).toBe(80);
    envVars.castErrorBreakMatches = true;
  });

  it('Strict cast number', async () => {
    envVars.strictCastNumbers = false;
    expect(envVars.int('LISTEN_PORT', 3000)).toBe(80);
    envVars.strictCastNumbers = true;
  });

  it('Cast Error Break Matches', async () => {
    envVars.castErrorBreakMatches = false;
    expect(envVars.int(['LISTEN_PORT', 'HTTP_LISTEN_PORT'], 3000)).toBe(80);
    envVars.castErrorBreakMatches = true;
  });

  it('Throw or Log Cast Error', async () => {
    envVars.throwOrLogCastError = (error) => {
      throw error;
    };
    let errorThrown: Error | undefined;
    try {
      expect(envVars.int('LISTEN_PORT', 3000)).toBe(80);
    } catch(error) {
      errorThrown = error;
    }
    expect(errorThrown).toBeDefined();
    envVars.throwOrLogCastError = undefined;
  });
});
