const booleanTrueValues = [
  'true',
  't',
  '1',
  'on',
  'enable',
  'enabled',
];

const booleanFalseValues = [
  'false',
  'f',
  '0',
  'off',
  'disable',
  'disabled',
];

export interface CastMap {
  value: string;
  string: string;
  integer: number;
  number: number;
  boolean: boolean;
  json: any;
}

export interface ThrowOrLogCastErrorFallback {
  (error: Error, type: keyof CastMap, name: string, value: string): void;
}

export class EnvVars {

  constructor(readonly env: { [name: string]: string | undefined } = process.env) {}

  /**
   * Throw or log cast error
   */
  throwOrLogCastError?: ThrowOrLogCastErrorFallback;
  /**
   * Rather it should break name matches and return default value in case of a casting error
   */
  castErrorBreakMatches: boolean = true;
  /**
   * Rather number casting should be strict
   */
  strictCastNumbers: boolean = true;

  protected castValueTo<K extends keyof CastMap, T extends CastMap[K] | undefined = CastMap[K] | undefined>(names: string | string[], type: K, defaultValue?: T): CastMap[K] | T {
    if (typeof names === 'string') {
      return this.castValueTo([names], type, defaultValue);
    }
    for (const name of names) {
      const value = this.env[name];
      if (value != null) {
        if (type === 'value') {
          return value;
        }
        if (type !== 'json') {
          const stringValue = value.trim().replace(/^"(.+)"$/, '$1');
          if (type === 'number' || type === 'integer') {
            const number = this.strictCastNumbers
              ? Number(stringValue)
              : parseFloat(stringValue);
            if (!isNaN(number)) {
              if (type === 'number') {
                return number;
              }
              if (type === 'integer') {
                if (!this.strictCastNumbers) {
                  return typeof Math.trunc === 'function'
                    ? Math.trunc(number)
                    : parseInt(`${number}`, 10);
                }
                if (number === parseInt(stringValue, 10)) {
                  return number;
                }
                if (this.throwOrLogCastError != null) {
                  this.throwOrLogCastError(new Error('Expected integer value'), type, name, stringValue);
                }
                if (this.castErrorBreakMatches) {
                  break;
                }
              }
            } else {
              if (this.throwOrLogCastError != null) {
                this.throwOrLogCastError(new Error('Invalid number'), type, name, stringValue);
              }
              if (this.castErrorBreakMatches) {
                break;
              }
            }
          }
          if (type === 'boolean') {
            const booleanValue = stringValue.toLowerCase();
            if (booleanTrueValues.indexOf(booleanValue) >= 0) {
              return true;
            }
            if (booleanFalseValues.indexOf(booleanValue) >= 0) {
              return false;
            }
            if (this.throwOrLogCastError != null) {
              this.throwOrLogCastError(new Error('Not valid boolean value'), type, name, stringValue);
            }
            if (this.castErrorBreakMatches) {
              break;
            }
          }
        } else {
          try {
            return JSON.parse(value);
          } catch (error) {
            if (this.throwOrLogCastError != null) {
              this.throwOrLogCastError(error, type, name, value);
            }
            if (this.castErrorBreakMatches) {
              break;
            }
          }
        }
      }
    }
    return defaultValue;
  }

  /**
   * Gets a original string environment variable by their given name.
   *
   * @param  {String} name - The name of the environment variable.
   * @param  {String} defaultVal - The default value to use.
   *
   * @return {String} The value.
   */
  value<T extends string | undefined = string | undefined>(name: string, defaultVal?: T): string | T;
  /**
   * Gets a original string environment variable by their given names (in matching order).
   *
   * @param  {String} names - The names of the environment variables.
   * @param  {String} defaultVal - The default value to use.
   *
   * @return {String} The value.
   */
  value<T extends string | undefined = undefined>(names: string | string[], defaultVal?: T): string | T;
  value(names: string | string[], defaultVal?: string): string | undefined {
    return this.castValueTo(names, 'value', defaultVal);
  }

  /**
   * Gets a string environment variable by their given name.
   *
   * @param  {String} name - The name of the environment variable.
   * @param  {String} defaultVal - The default value to use.
   *
   * @return {String} The value.
   */
  string<T extends string | undefined = string | undefined>(name: string, defaultVal?: T): string | T;

  /**
   * Gets a string environment variable by their given names (in matching order).
   *
   * @param  {String} names - The names of the environment variables.
   * @param  {String} defaultVal - The default value to use.
   *
   * @return {String} The value.
   */
  string<T extends string | undefined = undefined>(names: string | string[], defaultVal?: T): string | T;
  string(names: string | string[], defaultVal?: string): string | undefined {
    return this.castValueTo(names, 'string', defaultVal);
  }

  /**
   * Gets a number environment variable by their given name.
   * Notice: It do not verify NaN
   *
   * @param  {String} name - The name of the environment variable.
   * @param  {number} defaultVal - The default value to use.
   *
   * @return {number} The value.
   */
  number<T extends number | undefined = number | undefined>(name: string, defaultVal?: T): number;
  /**
   * Gets a number environment variable by their given  names (in matching order).
   * Notice: It do not verify NaN
   *
   * @param  {String} names - The name of the environment variables.
   * @param  {number} defaultVal - The default value to use.
   *
   * @return {number} The value.
   */
  number<T extends number | undefined = number | undefined>(names: string | string[], defaultVal?: T): number | T;
  number(names: string | string[], defaultVal?: number): number | undefined {
    return this.castValueTo(names, 'number', defaultVal);
  }


  /**
   * Gets a integer environment variable by their given name.
   * @param  {String} name - The name of the environment variable.
   * @param  {number} defaultVal - The default value to use.
   *
   * @return {number} The value.
   */
  int<T extends number | undefined = number | undefined>(name: string, defaultVal?: T): number | T;
  /**
   * Gets a integer environment variable by their given name. (in matching order).
   * @param  {String} names - The names of the environment variable.
   * @param  {number} defaultVal - The default value to use.
   *
   * @return {number} The value.
   */
  int<T extends number | undefined = number | undefined>(names: string | string[], defaultVal?: T): number | T;
  int(names: string | string[], defaultVal?: number): number | undefined {
    return this.castValueTo(names, 'integer', defaultVal);
  }

  /**
   * Gets a boolean environment variable by their given name.
   * @param  {String} name - The name of the environment variable.
   * @param  {number} defaultVal - The default value to use.
   *
   * @return {number} The value.
   */
  bool<T extends boolean | undefined = boolean | undefined>(name: string, defaultVal?: T): boolean | T;
  /**
   * Gets a boolean environment variable by their given name. (in matching order).
   * @param  {String} names - The names of the environment variable.
   * @param  {number} defaultVal - The default value to use.
   *
   * @return {number} The value.
   */
  bool<T extends boolean | undefined = boolean | undefined>(names: string | string[], defaultVal?: T): boolean | T;
  bool(names: string | string[], defaultVal?: boolean): boolean | undefined {
    return this.castValueTo(names, 'boolean', defaultVal);
  }

  /**
   * Parses a environment variable as json
   * @param name - The name of the environment variable.
   * @param defaultVal - The default value to use.
   * @param [throwOrLogError = false] - If should throw parsing error
   *
   * @return The value.
   */
  json(name: string, defaultVal?: any): any;
  /**
   * Parses a environment variable as json
   * @param names - The names of the environment variable.
   * @param defaultVal - The default value to use.
   * @param [throwOrLogError = false] - If should throw parsing error
   *
   * @return The value.
   */
  json(names: string | string[], defaultVal?: any): any {
    return this.castValueTo(names, 'json', defaultVal);
  }
}